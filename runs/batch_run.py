import subprocess
import pandas as pd

input_cloud = '../assets/cloud_cabin_section_1.ply'

octree_resolutions = [
    10,
    1.0,
    .5,
    .1,
    .05,
    .01,
    .005,
    .001
]

point_resolutions = [
    10,
    1.0,
    .5,
    .1,
    .05,
    .01,
    .005,
    .001
]


class CompressParameters:

    def __init__(self):
        self.octre_res = None
        self.point_res = None
        self.decoded_points = None
        self.size_on_disk = None
        self.compression_ratio = None


results = []


def process_output(stdout: str, octree_res: float, point_res: float) -> CompressParameters:
    parameters = CompressParameters()
    parameters.octre_res = octree_res
    parameters.point_res = point_res
    for line in stdout.split('\n'):
        line = line.strip('[0;m')
        if line.startswith('Number of decoded points'):
            number = line.split(sep=': ')[1]
            parameters.decoded_points = int(number)
        elif line.startswith('Size of compressed point cloud'):
            number = line.split(sep=': ')[1]
            parameters.size_on_disk = float(number.strip('kBytes'))
        elif line.startswith('Compression ratio'):
            number = line.split(sep=': ')[1]
            parameters.compression_ratio = float(number)

    return parameters


def main():
    df = pd.DataFrame(columns=['octre_res',
                               'point_res',
                               'decoded_points',
                               'size_on_disk (kBytes)',
                               'compression_ratio'])

    for octree_res in octree_resolutions:
        for point_res in point_resolutions:
            print('Processing with octree res {octree} and point res {point}'.format(octree=octree_res,
                                                                                     point=point_res))

            process = subprocess.Popen(['./compress_compare',
                                        '-cam', 'camera.cam',
                                        '-octree_res', str(octree_res),
                                        '-point_res', str(point_res),
                                        '-input_cloud', input_cloud],
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE,
                                       universal_newlines=True)

            output, errors = process.communicate()

            parameters = process_output(output, octree_res=octree_res, point_res=point_res)

            df = df.append({
                'octre_res': parameters.octre_res,
                'point_res': parameters.point_res,
                'decoded_points': parameters.decoded_points,
                'size_on_disk (kBytes)': parameters.size_on_disk,
                'compression_ratio': parameters.compression_ratio
            }, ignore_index=True)

    df.to_csv('result.csv', sep=',', index=True, encoding='utf-8')


if __name__ == '__main__':
    main()

"""
Esempio di info sulla compressione. Ovviamente non tutti questi dati sono importanti
Number of decoded points: 29
XYZ compression percentage: 298.563232%
XYZ bytes per point: 35.827587 bytes
Color compression percentage: 0.000000%
Color bytes per point: 0.000000 bytes
Size of uncompressed point cloud: 0.453125 kBytes
Size of compressed point cloud: 1.014648 kBytes
Total bytes per point: 35.827587 bytes
Total compression percentage: 223.922424%
Compression ratio: 0.446583
"""
