#ifndef TEST_OCTREE_OCTREE_VIEWER_H
#define TEST_OCTREE_OCTREE_VIEWER_H

#include <thread>

#include <pcl/io/auto_io.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/visualization/common/common.h>

#include <pcl/octree/octree_pointcloud_voxelcentroid.h>
#include <pcl/common/centroid.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkCubeSource.h>
#include <vtkCleanPolyData.h>

using namespace std::chrono_literals;

class OctreeViewer {
public:
    OctreeViewer(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud1,
                 pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud2,
                 double octree_resolution,
                 const char *name,
                 int argc,
                 char** argv);

    ~OctreeViewer();

    bool run();

private:
    //visualizer
    pcl::visualization::PCLVisualizer viz;
    //original cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_original;
    // compressed cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_compress;
    //displayed_cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr displayCloud;
    // cloud which contains the voxel center
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudVoxel;
    //octree
    pcl::octree::OctreePointCloudVoxelCentroid<pcl::PointXYZ> octree;
    // vista destra originale
    int v1;
    std::string cloud_original_id;
    // vista sinistra compress
    int v2;
    std::string cloud_compress_id;
    //level
    int displayed_depth;

    int text_height = 30;
    int screenshot_index = 0;
    float point_size_v1 = 0;
    float point_size_v2 = 0;
    int cube_line_size;
    bool bg_white;

    //bool to decide what should be display
    bool wireframe;
    bool show_cubes_, show_centroids_, show_original_points_, show_helper_;

    const char* window_name;

    void keyboard_event_occurred(const pcl::visualization::KeyboardEvent &event, void *);

    void show_legend();

    void show_help();

    void update();

    void clear_view();

    void create_cubes(double voxelSideLen, int viewport);

    void extract_points_at_level(int depth);

    bool increment_level();

    bool decrement_level();
};

#endif //TEST_OCTREE_OCTREE_VIEWER_H
