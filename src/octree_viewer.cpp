#include "../include/octree_viewer.h"

#include <cstdio>

#include <thread>

#include <pcl/io/auto_io.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/visualization/common/common.h>

#include <pcl/octree/octree_pointcloud_voxelcentroid.h>
#include <pcl/common/centroid.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkCubeSource.h>
#include <vtkCleanPolyData.h>

#include <boost/filesystem.hpp>

using namespace std::chrono_literals;


OctreeViewer::OctreeViewer(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud1,
                           pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud2,
                           double octree_resolution,
                           const char *window_name,
                           int argc,
                           char **argv) :
        viz(argc, argv, window_name),
        cloud_original(cloud1),
        cloud_compress(cloud2),
        octree(octree_resolution),
        v1(0), v2(0),
        cloud_original_id("cloud1"),
        cloud_compress_id("cloud2"),
        displayCloud(new pcl::PointCloud<pcl::PointXYZ>()),
        cloudVoxel(new pcl::PointCloud<pcl::PointXYZ>()),
        displayed_depth(0),
        wireframe(true),
        show_cubes_(false),
        show_centroids_(false),
        show_original_points_(true),
        show_helper_(false),
        window_name(window_name),
        point_size_v1(5.0),
        point_size_v2(5.0),
        cube_line_size(4),
        bg_white(true) {

    octree.setInputCloud(cloud2);
    //update bounding box automatically
    octree.defineBoundingBox();
    //add points in the tree
    octree.addPointsFromInputCloud();
    //register keyboard callbacks
    viz.registerKeyboardCallback(&OctreeViewer::keyboard_event_occurred, *this, nullptr);

    // left and right view
    viz.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
    viz.createViewPort(0.5, 0.0, 1.0, 1.0, v2);

    //set current level to half the maximum one
    displayed_depth = octree.getTreeDepth();
    if (displayed_depth > 6) {
        displayed_depth = static_cast<int> (floor(octree.getTreeDepth() / 2.0));
    } else if (displayed_depth == 0) {
        displayed_depth = 1;
    }

    //show octree at default depth
    extract_points_at_level(displayed_depth);

    //reset camera
    viz.resetCameraViewpoint("cloud");
}

OctreeViewer::~OctreeViewer() = default;

bool OctreeViewer::run() {
    while (!viz.wasStopped()) {
        viz.spinOnce(100);
        std::this_thread::sleep_for(200ms);
    }
    return true;
}

void OctreeViewer::keyboard_event_occurred(const pcl::visualization::KeyboardEvent &event, void *) {

    if (event.getKeySym() == "a" && event.keyDown()) {
        increment_level();
    } else if (event.getKeySym() == "z" && event.keyDown()) {
        decrement_level();
    } else if (event.getKeyCode() == 0x3f && event.keyDown()) {
        // 0x3f -> ? (question mark)
        show_helper_ = !show_helper_;
        update();
    } else if (event.getKeySym() == "v" && event.keyDown()) {
        show_cubes_ = !show_cubes_;
        update();
    } else if (event.getKeySym() == "b" && event.keyDown()) {
        show_centroids_ = !show_centroids_;
        update();
    } else if (event.getKeySym() == "t" && event.keyDown()) {
        show_original_points_ = !show_original_points_;
        update();
    } else if (event.getKeySym() == "w" && event.keyDown()) {
        wireframe = !wireframe;
        update();
    } else if (event.getKeySym() == "d" && event.keyDown()) {
        boost::filesystem::create_directories("screenshot");
        char file_name[256];
        sprintf(file_name, "screenshot/%s_%d.png", window_name, screenshot_index);
        screenshot_index++;
        viz.saveScreenshot(file_name);
        update();
    } else if ((event.getKeyCode() == '-') && event.keyDown()) {
        point_size_v1 = std::max(1.0f, point_size_v1 * (1 / 2.0f));
        point_size_v2 = std::max(1.0f, point_size_v2 * (1 / 2.0f));
        update();
    } else if ((event.getKeyCode() == '+') && event.keyDown()) {
        point_size_v1 *= 2.0f;
        point_size_v2 *= 2.0f;
        update();
    } else if ((event.getKeyCode() == ',') && event.keyDown()) {
        point_size_v2 = std::max(1.0f, point_size_v2 * (1 / 2.0f));
        update();
    } else if ((event.getKeyCode() == '.') && event.keyDown()) {
        point_size_v2 *= 2.0f;
        update();
    } else if ((event.getKeyCode() == 'n') && event.keyDown()) {
        point_size_v1 = std::max(1.0f, point_size_v1 * (1 / 2.0f));
        update();
    } else if ((event.getKeyCode() == 'm') && event.keyDown()) {
        point_size_v1 *= 2.0f;
        update();
    } else if ((event.getKeySym() == "l") && event.keyDown()) {
        ++cube_line_size;
        update();
    } else if ((event.getKeySym() == "k") && event.keyDown()) {
        cube_line_size = std::max(1, --cube_line_size);
        update();
    } else if ((event.getKeySym() == "i") && event.keyDown()) {
        bg_white = !bg_white;
        update();
    } else if ((event.getKeySym() == "y") && event.keyDown()) {
        viz.removePointCloud(cloud_original_id, v1);
    }
}

void OctreeViewer::show_legend() {

    int step = 35;
    int offset = 5;

    float red, blue, green, full;

    if (bg_white) {
        red = 0.0;
        blue = 0.4;
        green = 0.4;
        full = 0.0;
    } else {
        red = 1.0;
        blue = 0.8;
        green = 0.8;
        full = 1.0;
    }

    char level[256];
    sprintf(level, "Displayed depth is %d on %d", displayed_depth, octree.getTreeDepth());
    viz.removeShape("level_t1");
    viz.addText(level, 10, offset + step * 2, 30, red, blue, green, "level_t1", v1);

    viz.removeShape("level_t2");
    sprintf(level, "Voxel size: %.4fm [%lu voxels]", std::sqrt(octree.getVoxelSquaredSideLen(displayed_depth)),
            cloudVoxel->points.size());
    viz.addText(level, 10, offset + step * 1, 30, red, blue, green, "level_t2", v1);

    viz.addText(window_name, 10, offset + step * 0, text_height, full, full, full, "w_name", v1);

}

void OctreeViewer::show_help() {

    int *window_size = viz.getRenderWindow()->GetSize();

    float red, blue, green;

    if (bg_white) {
        red = 0.4;
        blue = 0.0;
        green = 0.4;
    } else {
        red = 0.8;
        blue = 1.0;
        green = 0.8;
    }

    //key legends
    viz.addText("Keys:",
                10, window_size[1] - (text_height * 1 + 5), text_height, red, blue, green, "keys_t", v1);
    viz.addText("h -> Help:",
                10, window_size[1] - (text_height * 2 + 5), text_height, red, blue, green, "key_h", v1);
    viz.addText("a -> Increment displayed depth",
                10, window_size[1] - (text_height * 3 + 5), text_height, red, blue, green, "key_a_t", v1);
    viz.addText("z -> Decrement displayed depth",
                10, window_size[1] - (text_height * 4 + 5), text_height, red, blue, green, "key_z_t", v1);
    viz.addText("v -> Toggle octree cubes representation",
                10, window_size[1] - (text_height * 5 + 5), text_height, red, blue, green, "key_v_t", v1);
    viz.addText("b -> Toggle centroid points representation",
                10, window_size[1] - (text_height * 6 + 5), text_height, red, blue, green, "key_b_t", v1);
    viz.addText("n -> Toggle original point cloud representation",
                10, window_size[1] - (text_height * 7 + 5), text_height, red, blue, green, "key_n_t", v1);
    viz.addText(", . -> Decrease/Increase point size for right view",
                10, window_size[1] - (text_height * 8 + 5), text_height, red, blue, green, "key_dot_t", v1);
    viz.addText("n m -> Decrease/Increase point size for left view",
                10, window_size[1] - (text_height * 9 + 5), text_height, red, blue, green, "key_mn_t", v1);

}

void OctreeViewer::update() {

    clear_view();

    float black = 0.0;
    float white = 1.0;

    if (bg_white) viz.setBackgroundColor(white, white, white);
    else viz.setBackgroundColor(black, black, black);

    if (show_helper_) {
        show_help();
    }

    if (show_cubes_) {
        // show octree as cubes
        create_cubes(std::sqrt(octree.getVoxelSquaredSideLen(displayed_depth)), v1);
        create_cubes(std::sqrt(octree.getVoxelSquaredSideLen(displayed_depth)), v2);
    }

    if (show_centroids_) {
        // show centroid points
        pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZ> color_handler(cloudVoxel, "x");

        viz.addPointCloud(cloudVoxel, color_handler, "cloud_centroid1", v1);
        viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v1,
                                             "cloud_centroid1", v1);

        viz.addPointCloud(cloudVoxel, color_handler, "cloud_centroid2", v2);
        viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v2,
                                             "cloud_centroid2", v2);
    }

    if (show_original_points_) {

        if (bg_white) {
            // original point cloud
            viz.addPointCloud(cloud_original, cloud_original_id, v1);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v1,
                                                 cloud_original_id, v1);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, black, black, black,
                                                 cloud_original_id, v1);

            // compressed point cloud
            viz.addPointCloud(cloud_compress, cloud_compress_id, v2);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v2,
                                                 cloud_compress_id, v2);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, black, black, black,
                                                 cloud_compress_id, v2);

        } else {
            // original point cloud
            viz.addPointCloud(cloud_original, cloud_original_id, v1);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v1,
                                                 cloud_original_id, v1);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, white, white, white,
                                                 cloud_original_id, v1);

            // compressed point cloud
            viz.addPointCloud(cloud_compress, cloud_compress_id, v2);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_v2,
                                                 cloud_compress_id, v2);
            viz.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, white, white, white,
                                                 cloud_compress_id, v2);
        }
    }

    show_legend();

}

void OctreeViewer::clear_view() {

    // remove cubes if any
    vtkRenderer *renderer = viz.getRenderWindow()->GetRenderers()->GetFirstRenderer();
    while (renderer->GetActors()->GetNumberOfItems() > 0)
        renderer->RemoveActor(renderer->GetActors()->GetLastActor());
    // remove point clouds if any
    viz.removeAllPointClouds();
    viz.removeAllShapes();

}


void OctreeViewer::create_cubes(double voxelSideLen, int viewport) {

    float red, blue, green;

    red = 0.5;
    blue = 0.5;
    green = 0.5;

    // Create every cubes to be displayed
    double s = voxelSideLen / 2.0;
    int i = 0;
    for (auto &point : cloudVoxel->points) {
        double x = point.x;
        double y = point.y;
        double z = point.z;
        std::string cube_name = "cube" + std::to_string(i) + std::to_string(random());
        viz.addCube(x - s, x + s, y - s, y + s, z - s, z + s, red, green, blue, cube_name, viewport);
        ++i;
        viz.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, cube_line_size, cube_name);
    }
    viz.setRepresentationToWireframeForAllActors();
}


void OctreeViewer::extract_points_at_level(int depth) {
    std::cout << "extractPointsAtLevel" << std::endl;
    displayCloud->points.clear();   // pcl::PointCloud<pcl::PointXYZ>::Ptr
    cloudVoxel->points.clear();     // pcl::PointCloud<pcl::PointXYZ>::Ptr

    pcl::PointXYZ pt_voxel_center;
    pcl::PointXYZ pt_centroid;
    std::cout << "===== Extracting data at depth " << depth << "... " << std::flush;
    double start = pcl::getTime();

    for (pcl::octree::OctreePointCloudVoxelCentroid<pcl::PointXYZ>::FixedDepthIterator tree_it = octree.fixed_depth_begin(
            static_cast<unsigned int>(depth));
         tree_it != octree.fixed_depth_end();
         ++tree_it) {
        // Compute the point at the center of the voxel which represents the current OctreeNode
        Eigen::Vector3f voxel_min, voxel_max;
        octree.getVoxelBounds(tree_it, voxel_min, voxel_max);

        pt_voxel_center.x = (voxel_min.x() + voxel_max.x()) / 2.0f;
        pt_voxel_center.y = (voxel_min.y() + voxel_max.y()) / 2.0f;
        pt_voxel_center.z = (voxel_min.z() + voxel_max.z()) / 2.0f;
        cloudVoxel->points.push_back(pt_voxel_center);

        // If the asked depth is the depth of the octree, retrieve the centroid at this LeafNode
        if (octree.getTreeDepth() == (unsigned int) depth) {
            auto *container = dynamic_cast<pcl::octree::OctreePointCloudVoxelCentroid<pcl::PointXYZ>::LeafNode *> (tree_it.getCurrentOctreeNode());

            container->getContainer().getCentroid(pt_centroid);
        }
            // Else, compute the centroid of the LeafNode under the current BranchNode
        else {
            // Retrieve every centroid under the current BranchNode
            pcl::octree::OctreeKey dummy_key;
            pcl::PointCloud<pcl::PointXYZ>::VectorType voxelCentroids;
            octree.getVoxelCentroidsRecursive(
                    dynamic_cast<pcl::octree::OctreePointCloudVoxelCentroid<pcl::PointXYZ>::BranchNode *> (*tree_it),
                    dummy_key, voxelCentroids);

            // Iterate over the leafs to compute the centroid of all of them
            pcl::CentroidPoint<pcl::PointXYZ> centroid;
            for (auto voxelCentroid : voxelCentroids) {
                centroid.add(voxelCentroid);
            }
            centroid.get(pt_centroid);
        }

        displayCloud->points.push_back(pt_centroid);
    }

    double end = pcl::getTime();
    printf("%lu pts, %.4gs. %.4gs./pt. =====\n", displayCloud->points.size(), end - start,
           (end - start) / static_cast<double> (displayCloud->points.size()));

    update();
}

bool OctreeViewer::increment_level() {
    if (displayed_depth < static_cast<int> (octree.getTreeDepth())) {
        displayed_depth++;
        extract_points_at_level(displayed_depth);
        return true;
    } else
        return false;
}

bool OctreeViewer::decrement_level() {
    if (displayed_depth > 0) {
        displayed_depth--;
        extract_points_at_level(displayed_depth);
        return true;
    }
    return false;
}
