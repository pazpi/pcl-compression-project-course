#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <thread>
#include <filesystem>

#include <pcl/common/centroid.h>
#include <pcl/console/parse.h>
#include <pcl/io/auto_io.h>
#include <pcl/filters/filter.h>
#include <pcl/compression/octree_pointcloud_compression.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <vtkCubeSource.h>
#include <vtkCleanPolyData.h>

#include "../include/octree_viewer.h"

void print_usage(const char *, char **);

double *linspace(double, double, int);

bool load_cloud(const char *filename, const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);

void create_cloud(float, float, int, const pcl::PointCloud<pcl::PointXYZ>::Ptr &);

char usage[] = "\n"
               "  usage: %s [parameters]\n"
               "\n"
               "  optional compression parameters:\n"
               "      -h/--help                  : help message\n"
               "      -point_res   [precision]   : point precision - default 0.8\n"
               "      -octree_res  [precision]   : octree voxel size - default 1.0\n"
               "      -input_cloud [path]        : cloud on disk - default autogenerate\n"
               "      -save_path   [path]        : folder in which save output cloud - default \"./cloud\"\n"
               "      -no_vis                    : show point cloud comparison before-after compression\n"
               "\n"
               "  example:\n"
               "      %s -point_res 0.01 -octree_res 0.1 -save_path cloud_compressed.pcc \n"
               "\n";

int main(int argc, char **argv) {

    if (pcl::console::find_argument(argc, argv, "-h") > 0) {
        print_usage(usage, argv);
        return EXIT_SUCCESS;
    } else if (pcl::console::find_argument(argc, argv, "--help") > 0) {
        print_usage(usage, argv);
        return EXIT_SUCCESS;
    }


    std::string window_name;

    // default values
    std::filesystem::path cloud_path = "../cloud";
    // std::string name_compress = "cloud_compressed.pcc";
    bool showStatistics = true;
    double pointResolution = 0.8f;  // min size of point resolution
    float octreeResolution = 1.f;   // Voxel size
    bool doVoxelGridDownDownSampling = false;
    unsigned int iFrameRate = 30;
    bool doColorEncoding = false;
    unsigned int colorBitResolution = 6;
    pcl::io::compression_Profiles_e compressionProfile = pcl::io::MANUAL_CONFIGURATION;


    if (pcl::console::find_argument(argc, argv, "-save_path") > 0) {
        std::string cloud_path_temp;
        pcl::console::parse_argument(argc, argv, "-save_path", cloud_path_temp);
        cloud_path = cloud_path_temp;
    }

    // Genero la point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new(pcl::PointCloud<pcl::PointXYZ>));
    if (pcl::console::find_argument(argc, argv, "-input_cloud") > 0) {
        std::string input_cloud;
        pcl::console::parse_argument(argc, argv, "-input_cloud", input_cloud);
        load_cloud(input_cloud.c_str(), cloud);
        window_name = std::filesystem::path(input_cloud).filename();
    } else {
        float min = -1.0;
        float max = 1.0;
        int n_division = 11;
        create_cloud(min, max, n_division, cloud);
        window_name = "Autogenerate";
        // TRUE->binary - FALSE->ASCII
        std::string cloud_path_autogenerate = cloud_path / "cloud_autogenerate.pcd";
        pcl::io::savePCDFile(cloud_path_autogenerate, *cloud, false);
    }

    // compress and save compressed point cloud .pcc
    pcl::console::parse_argument(argc, argv, "-octree_res", octreeResolution);
    pcl::console::parse_argument(argc, argv, "-point_res", pointResolution);

    pcl::io::OctreePointCloudCompression<pcl::PointXYZ>::Ptr octreeCoder(
            new pcl::io::OctreePointCloudCompression<pcl::PointXYZ>(compressionProfile, showStatistics,
                                                                    pointResolution,
                                                                    octreeResolution, doVoxelGridDownDownSampling,
                                                                    iFrameRate,
                                                                    doColorEncoding,
                                                                    static_cast<unsigned char> (colorBitResolution)));

    fprintf(stderr, "[INFO] Point resolution %.5f\n", pointResolution);

    // Save compress cloud
    std::ofstream compressedPCFile;
    std::string cloud_path_compress = cloud_path /
                                      ("cloud_compress_octreeres_" + std::to_string(octreeResolution) + "_pointres_" +
                                       std::to_string(pointResolution) + ".pcc");
    compressedPCFile.open(cloud_path_compress, std::ios::out | std::ios::trunc | std::ios::binary);
    octreeCoder->encodePointCloud(cloud, compressedPCFile);
    fprintf(stderr, "[INFO] Octree resolution %.5f, octree depth %d\n", octreeResolution, octreeCoder->getTreeDepth());

    // read compress cloud and save it as ASCII un-compressed
    ifstream PCFile;
    PCFile.open(cloud_path_compress, ios::in | ios::binary);
    PCFile.seekg(0);
    PCFile.unsetf(ios_base::skipws);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_uncompressed(new pcl::PointCloud<pcl::PointXYZ>());
    octreeCoder->decodePointCloud(PCFile, cloud_uncompressed);
    std::string cloud_path_uncompress = cloud_path / ("cloud_uncompress_octreeres_" + std::to_string(octreeResolution) +
                                                      "_pointres_" + std::to_string(pointResolution) + ".pcd");
    pcl::io::savePCDFile(cloud_path_uncompress, *cloud_uncompressed, false);

    // open view
    if (!pcl::console::find_switch(argc, argv, "-no_vis")) {

        window_name = window_name + " - Octree res: " + std::to_string(octreeResolution) + "m - Point res: " +
                      std::to_string(pointResolution) + "m";

        OctreeViewer v(cloud, cloud_uncompressed, octreeResolution, window_name.c_str(), argc, argv);

        return v.run();
    }

    return EXIT_SUCCESS;
}


void print_usage(const char *help_message, char **argv) {
    fprintf(stderr, help_message, argv[0], argv[0]);
}


double *linspace(double min, double max, int n_division) {
    auto *array = (double *) malloc(n_division * sizeof(double));

    float range = fabs(min) + fabs(max);
    float step_size = range / (n_division - 1);

    for (int i = 0; i < n_division; i++) {
        array[i] = min + step_size * i;
    }
    return array;
}


bool load_cloud(const char *filename, const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud) {
    printf("Loading file %s\n", filename);
    //read cloud
    if (pcl::io::load(filename, *cloud)) {
        return false;
    }

    //remove NaN Points
    std::vector<int> nanIndexes;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, nanIndexes);
    std::cout << "Loaded " << cloud->points.size() << " points" << std::endl;

    return true;
}


void create_cloud(float min, float max, int n_division, const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud) {

    double *array_x = linspace(min, max, n_division);
    double *array_y = linspace(min, max, n_division);
    double *array_z = linspace(min, max, n_division);

    cloud->width = (int) pow(n_division, 3);
    cloud->height = 1;
    cloud->points.resize(cloud->width * cloud->height);

    for (size_t i = 0; i < cloud->points.size(); ++i) {
        cloud->points[i].x = array_x[(i / (n_division * n_division)) % n_division];
        cloud->points[i].y = array_y[(i / n_division) % n_division];
        cloud->points[i].z = array_z[i % n_division];
    }

    free(array_x);
    free(array_y);
    free(array_z);
}
