#include <pcl/io/auto_io.h>
#include <pcl/filters/filter.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/compression/octree_pointcloud_compression.h>

#include <stdio.h>
#include <sstream>
#include <stdlib.h>


bool load_cloud(char *filename, const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &);

int main(int argc, char** argv){

    printf("Start - init variables\n");
    bool show_statistics = true;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr original_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr compress_cloud(new pcl::PointCloud<pcl::PointXYZRGB> ());

    pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB>* point_cloud_encoder;
    pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB>* point_cloud_decoder;

    // for a full list of profiles see: /io/include/pcl/compression/compression_profiles.h
    pcl::io::compression_Profiles_e compression_profile = pcl::io::LOW_RES_ONLINE_COMPRESSION_WITH_COLOR; // mm^3

    // instantiate point original_cloud compression for encoding and decoding
    point_cloud_encoder = new pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB> (compression_profile, show_statistics);
    point_cloud_decoder = new pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB> ();

    printf("Load original_cloud from disk\n");
    load_cloud((char*)"cloud_cabin.pcd", original_cloud);

    printf("Encode...\n");
    std::stringstream compressedData;
    point_cloud_encoder->encodePointCloud( original_cloud, compressedData );

    printf("Decode...\n");
    point_cloud_decoder->decodePointCloud (compressedData, compress_cloud);

    printf("Save compres original_cloud\n");
    // TRUE->binary - FALSE->ASCII
	pcl::io::savePCDFile((char*)"cloud_cabin_compressed.pcd", *compress_cloud, true);
	pcl::io::savePLYFile((char*)"cloud_cabin_compressed.ply", *compress_cloud, false);
    pcl::io::save((char*)"cloud_cabin_compressed-binary.ply", *compress_cloud);
    // pcl::io::save((char*)"cloud_cabin_compressed.pcd", *compress_cloud);
    printf("End\n");

}


bool load_cloud(char *filename, const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud) {
    printf("Loading file %s", filename);
    //read cloud
    if (pcl::io::load(filename, *cloud)) {
        return false;
    }

    //remove NaN Points
    std::vector<int> nanIndexes;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, nanIndexes);
    std::cout << "Loaded " << cloud->points.size() << " points" << std::endl;

    return true;
}
