# Analisi di compressione di una point cloud con la libreria PCL

## Struttura del progetto

La cartella principale è `src`, qua dentro sono presenti i codici sorgenti dei
vari test eseguiti.

### `octree_compress.cpp`
Viene usato per comprimere una point cloud in ingresso
usando un profilo di compressione predefinito dalla libreria PCL.

### `compress_compare.ccp`
Questo è il cuore del progetto. In questo file viene caricata
la point cloud dichiarata tramite il parametro `-input_cloud` altrimenti verrà
creata una point cloud di default creata in maniera artificiale.
Questa point cloud contine 100 punti che sono equispaziati in un cubo che copre
uno spazio che va da -1 a 1 nei tre punti cardinali.
Esistono poi altri parametri per impostare la compressione dell'octree e dei
punti.

Comunque per ogni caso lancindo il programma col parametro `-h` mostra i vari
parametri impostabili con relativa spiegazione.

### `octree_viewer.cpp` e `octree_viewer.h`
è la classe C++ principale che
è stata sviluppata per permettere di mostrare la differenza fra due point
cloud.
A destra la versione originale mentre a sinistra la versione compressa.
Esistono dei pulsanti preimpostati per gestire la visuale.

| Key   | Descrizione del comando                                           |
|-------|-------------------------------------------------------------------|
| ?     | Mostra alcuni comandi base                                        |
| `q`   | Chiude la finestra del programma                                  |
| `i`   | Inverti colori chiaro/scuro                                       |
| `a/z` | Aumenta / Diminuisce la profondità dell'octree visualizzato       |
| `v`   | Mostra/nascondi i confini dei voxel formati dall'octree           |
| `b`   | Mostra/nascondi centroidi dei voxel                               |
| `t`   | Mostra/nascondi la point cloud di destra                          |
| `y`   | Nascondi tutte le point cloud                                     |
| `w`   | Modalità __wireframe__ dei voxel                                  |
| `s`   | Modalità __solid__ dei voxel                                      |
| `d`   | Screenshot della schermata visualizzata                           |
| `+/-` | Aumenta / Diminuisce dimensioni dei punti per entrambe le nuvole  |
| `n/m` | Aumenta / Diminuisce dimensioni dei punti per la nuvola originale |
| `,/.` | Aumenta / Diminuisce dimensioni dei punti per la nuvola compressa |
| `k/l` | Aumenta / Diminuisce lo spessore del bordo del voxel visualizzato |

## Installazione

Creare la cartella `build` e compialare con `cmake`

```
mkdir build
cd build
cmake ..
```

## Run

Dentro la cartella `run` è presente uno script in Python3 per eseguire il programma
varie volte per differenti valori di octree e point resolution.
Prima di eseguire lo script è necessario copiare il file `compress_compare` compilato dentro la cartella `bin/`.
Per ottenere i risultati è necarrio installare il modulo Pandas. Per
installarlo basta installarlo col proprio metodo preferito.
Alla fine viene generato il file `result.csv` nella cartella dello script
contenente i risultati della compressione, fra cui:
    
    * Il numero di punti decodificati
    * Dimensione della nuvola compressa su disco
    * Fattore di compressione

Una cosa molto utile da impostare in questo script è il parametro `no_vis` che
impone di non visualizzare la comparazione fra la nuvola originale e quella
compressa.
Questo permette di ottenere solamente i risultati numerici senza dover chiudere
la finestra ad ogni iterazione.

## Note usate durante il progetto

### Resolution della point cloud in compressione OCTREE

#### Octree resolution
`octreeResolution` è la dimensione di un lato di un Voxel, il quale per sua rappresentazione è un cubo, quindi i tre lati
sono uguali.

Solitamente questo valore viene impostato ad un valore più alto rispetto alla __pointResolution__ questo perchè non vogliamo
creare troppi rami nella struttura ad albero in quanto la maggior parte di essi sarebbe vuota.

#### Point resolution
`pointResolution` è la risuluzione minima dei punti all'interno della point cloud.
Ottenuto il Voxel minimo creato grazie all'octree e a __octreeResolution__ i punti che cadono all'interno del cubo
vengono codificati.
Questa codifica sfrutta il fatto che questo Voxel minimo viene suddiviso in più parti, ognuna lunga __pointResolution__, creando un
numero fissato di cubetti ognuno con dimensione (__pointResolution__ * __pointResolution__ * __pointResolution__).
Ad esempio se `octreeResolution = 1` e `pointResolution = .1`, ogni lato del Voxel viene suddiviso in 10 (`1 / .1 = 10` ) parti
creando al suo interno 1000 cubetti.
Creati questi cubetti i punti che devono essere codificati vengnono salvati indicando solamente in quale cubetto ricadono,
salvando il numero di divisione sui rispettivi tre assi; riuscendo effettivamente a ridurre la risoluzione della point cloud
al valore di __pointResolution__.

## Link utili

http://www.pcl-users.org/Octree-resolution-td4028700.html

https://ieeexplore.ieee.org/abstract/document/6224647
